package mntkJavaPackage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.TimeZone;

public class TimeZoneTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] ids = TimeZone.getAvailableIDs();
		Random random = new Random();
		
		for(int i = 0; i<5; i++){
			printTime(ids[random.nextInt(ids.length)]);
		}
		printTime();
	}
	
	public static void printTime(){
		Date date = new GregorianCalendar().getTime();
		DateFormat format = 
				new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		TimeZone timezone = TimeZone.getDefault();
		format.setTimeZone(timezone);
		
		System.out.println(timezone.getDisplayName()+ ":" +
							format.format(date));
	}
	public static void printTime(String string){
		Date date = new GregorianCalendar().getTime();
		DateFormat format = 
				new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		TimeZone timezone = TimeZone.getTimeZone(string);
		format.setTimeZone(timezone);
		
		System.out.println(timezone.getDisplayName()+ ":" +
							format.format(date));
	}
}
